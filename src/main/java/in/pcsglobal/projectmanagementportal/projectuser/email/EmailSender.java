package in.pcsglobal.projectmanagementportal.projectuser.email;

public interface EmailSender {
    void send(String to, String email);
}
