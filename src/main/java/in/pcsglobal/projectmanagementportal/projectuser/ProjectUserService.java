package in.pcsglobal.projectmanagementportal.projectuser;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import in.pcsglobal.projectmanagementportal.registration.token.ConfirmationToken;
import in.pcsglobal.projectmanagementportal.registration.token.ConfirmationTokenService;
import lombok.AllArgsConstructor;


@Service
@AllArgsConstructor
public class ProjectUserService implements UserDetailsService{
	
	private final static String USER_NOT_FOUND_MSG= 
			"user with email %s not found";
	
	private final ProjectUserRepository projectUserRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final ConfirmationTokenService confirmationTokenService;

	@Override
	public UserDetails loadUserByUsername(String email) 
			throws UsernameNotFoundException {
		return projectUserRepository.findByEmail(email)
				.orElseThrow(() -> new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG, email)));
	}
	
	public String signUpUser(ProjectUser projectUser) {
		boolean userExists = projectUserRepository
				.findByEmail(projectUser.getEmail())
				.isPresent();
		
		if (userExists) {
			//TODO: if not confirmed, send email again
			throw new IllegalStateException("email already taken");
		}
		
		String encodedPassword = bCryptPasswordEncoder.encode(projectUser.getPassword());
		
		projectUser.setPassword(encodedPassword);
		
		projectUserRepository.save(projectUser);
		
		String token = UUID.randomUUID().toString();
		
		ConfirmationToken confirmationToken = new ConfirmationToken(
				token,LocalDateTime.now(),
				LocalDateTime.now().plusMinutes(15),
				projectUser
				);
		
		confirmationTokenService.saveConfirmationToken(confirmationToken);
		//TODO: Send Email
		return token;
	}

    public int enableProjectUser(String email) {
		return projectUserRepository.enableProjectUser(email);
    }

}
