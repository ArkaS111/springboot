package in.pcsglobal.projectmanagementportal.projectuser;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional(readOnly = true)
public interface ProjectUserRepository extends JpaRepository<ProjectUser, Long> {
	Optional<ProjectUser> findByEmail(String email);

	@Transactional
	@Modifying
	@Query("UPDATE ProjectUser a" + " SET a.enabled = TRUE WHERE a.email =?1")
    int enableProjectUser(String email);

}
