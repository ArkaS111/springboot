package in.pcsglobal.projectmanagementportal.projectuser;

public enum ProjectUserRole {
	USER,
	ADMIN
}
