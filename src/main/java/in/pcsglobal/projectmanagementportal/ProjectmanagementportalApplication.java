package in.pcsglobal.projectmanagementportal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectmanagementportalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectmanagementportalApplication.class, args);
	}

}
