package in.pcsglobal.projectmanagementportal.project;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Project implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, updatable = false )
	private Long id;
	private String name;
	private String category;
	private String status;
	
	public Project(String name, String category, String status) {
		this.name = name;
		this.category = category;
		this.status = status;
	}
	
	public Long getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getCategory() {
		return category;
	}
	public String getStatus() {
		return status;
	}
	
	@Override
	public String toString() {
		return "Employee{" +
				"id =" +id +
				",name='"+ name+'\''+
				",category='"+ category + '\'' +
				",status='" + status + '\''+
				'}';
		}

}
