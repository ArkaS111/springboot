package in.pcsglobal.projectmanagementportal.project;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectRepository extends JpaRepository<Project, Long>{
	
	void deleteProjectById(Long id);

	Optional<Project> findProjectById(Long id);
	

}
