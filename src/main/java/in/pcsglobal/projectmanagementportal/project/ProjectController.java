package in.pcsglobal.projectmanagementportal.project;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.pcsglobal.projectmanagementportal.project.Project;
import in.pcsglobal.projectmanagementportal.project.ProjectService;
import lombok.AllArgsConstructor;

@RestController
@RequestMapping(path = "projectmanagementportal/project")
@AllArgsConstructor
@ResponseBody
public class ProjectController {
	
	private final ProjectService projectService;

	
	@GetMapping("/all")
	public ResponseEntity<List<Project>> getAllProjects() {
		List<Project> projects = projectService.findAllProjects();
		return new ResponseEntity<>(projects, HttpStatus.OK);
	}
	
	@GetMapping("/find/{id}")
	public ResponseEntity<Project> getProjectById(@PathVariable("id") Long id) {
		Project project = projectService.findProjectById(id);
		return new ResponseEntity<>(project, HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<Project> addProject(@RequestBody Project project){
		Project newProject = projectService.addProject(project);
		return new ResponseEntity<>(project, HttpStatus.CREATED);
		//return "added";
	}
	
	@PutMapping("/update")
	public ResponseEntity<Project> updateProject(@RequestBody Project project){
		Project updateProject = projectService.updateProject(project);
		return new ResponseEntity<>(project, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteProject(@PathVariable("id") Long id){
		projectService.deleteProject(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
