package in.pcsglobal.projectmanagementportal.project;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.pcsglobal.projectmanagementportal.exception.ProjectNotFoundException;


@Service
public class ProjectService {
	private final ProjectRepository projectRepository;
	
	@Autowired
	public ProjectService(ProjectRepository projectRepository) {
		this.projectRepository = projectRepository;
	}
	
	public List<Project> findAllProjects() {
		return projectRepository.findAll();
	}
	
	public Project addProject(Project project) {
		return projectRepository.save(project);
	}
	
	public Project updateProject(Project project) {
		return projectRepository.save(project);
	}
	
	public Project findProjectById(Long id) {
		return projectRepository.findProjectById(id).orElseThrow(() -> new ProjectNotFoundException("Project by id" +id+ "not found"));
		
	}
	
	public void deleteProject(Long id) {
		projectRepository.deleteProjectById(id);
	}

}
